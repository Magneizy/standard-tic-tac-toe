def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(n):
    print_board(board)
    print("GAME OVER")
    print(board[n], "has won")
    exit()

def is_winner(n1, n2, n3):
    if board[n1] == board[n2] and board[n2] == board[n3]:
        return True


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_winner(0, 1, 2):
        game_over(0)
    elif is_winner(3, 4, 5):
        game_over(3)
    elif is_winner(6, 7, 8):
        game_over(6)
    elif is_winner(0, 3, 6):
        game_over(0)
    elif is_winner(1, 4, 7):
        game_over(1)
    elif is_winner(2, 5, 8):
        game_over(2)
    elif is_winner(0, 4, 8):
        game_over(0)
    elif is_winner(2, 4, 6):
        game_over(0)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
